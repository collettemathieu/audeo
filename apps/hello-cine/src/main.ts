/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger, ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app/app.module';
import { HttpExceptionFilter } from './app/common/filters/http-exception.filter';
import { ApiKeyGuard } from './app/common/guards/api-key.guard';
import { WrapResponseInterceptor } from './app/common/interceptors/wrap-response.interceptor';

import { environment } from './environments/environment';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = environment.production === true ? 'api' : '';
  app.setGlobalPrefix(globalPrefix);

  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new WrapResponseInterceptor());
  // app.useGlobalPipes(new ParseIntPipe());


    // Setting up Swagger document
    const options = new DocumentBuilder().setTitle('Hello Ciné').setDescription('Hello ciné application').setVersion('1.0').build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api', app, document);

  const port = process.env.PORT || 3333;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
