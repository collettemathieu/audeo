import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MoviesModule } from './movies/movies.module';
import { MovieRatingModule } from './movie-rating/movie-rating.module';
import { CommonModule } from './common/common.module';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import appConfig from './app.config';
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
      envFilePath: ['.env', '.env.local'],
      validationSchema: Joi.object({
        // TOTO: Joi.required(),
        POSTGRES_HOST: Joi.required(),
        POSTGRES_DB: Joi.required(),
        POSTGRES_PORT: Joi.number().default(5432)
      })
    }),
    MoviesModule,
    TypeOrmModule.forRootAsync({
      useFactory: () => {
        return {
          type: 'postgres', // type of our database
          host: process.env.POSTGRES_HOST, // database host
          port: +process.env.POSTGRES_PORT, // database host
          username: process.env.POSTGRES_USER, // username
          password: process.env.POSTGRES_PASSWORD, // user password
          database: process.env.POSTGRES_DB, // name of our database,
          autoLoadEntities: true, // models will be loaded automatically
          synchronize: true, // your entities will be synced with the database(recommended: disable in prod)
        }
      },
      inject: []
    }),
    MovieRatingModule,
    CommonModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
