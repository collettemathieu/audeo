import { Injectable, NestMiddleware, RequestMethod } from '@nestjs/common';
import { Response } from 'express';
import { Request } from 'express';


export const requestMethods = {
  'GET': 'GET' as const,
  'POST': 'POST' as const
};

export type RequestMethods = keyof typeof requestMethods;


export const requestMethodToInt: Record<RequestMethods, number> = {
  'GET': RequestMethod.GET,
  'POST': RequestMethod.POST
}

@Injectable()
export class PublicMiddleware implements NestMiddleware {
  use(request: Request, response: Response, next: () => void) {

    if(requestMethodToInt[request.method] === RequestMethod.GET){
      request.headers['authorization'] = process.env.API_KEY;
    }

    next();
  }
}
