import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC } from '../decorators/is-public.decorator';

@Injectable()
export class ApiKeyGuard implements CanActivate {

  constructor(private reflector: Reflector){}

  canActivate(
    context: ExecutionContext
  ) {

    const isPublic = this.reflector.get(IS_PUBLIC, context.getHandler());

    if(isPublic === true){
      return true;
    }

    const request = context.switchToHttp().getRequest();

    const authenticationHeader = request.header('Authorization');

    return authenticationHeader === process.env.API_KEY;
  }
}
