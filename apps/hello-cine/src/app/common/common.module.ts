import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { MoviesController } from '../movies/movies.controller';
import { ApiKeyGuard } from './guards/api-key.guard';
import { PublicMiddleware } from './middlewares/public.middleware';

@Module({
    providers: [{
        provide: APP_GUARD,
        useClass: ApiKeyGuard
    }]
})
export class CommonModule implements NestModule {

    configure(consumer: MiddlewareConsumer) {
        // consumer.apply(PublicMiddleware).forRoutes('*');
        consumer.apply(PublicMiddleware).forRoutes(MoviesController);
        // consumer.apply(PublicMiddleware).exclude('*');
        // consumer.apply(PublicMiddleware).forRoutes({
        //     path: 'movies',
        //     method: RequestMethod.GET
            
        // });
    }
}
