import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ParseIntPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata) {
    
    const valueParsed = parseInt(value, 10);

    if(isNaN(valueParsed) === true){
      throw new BadRequestException(`Validation failed. ${value} should be an interger`);
    }

    return valueParsed;
  }
}
