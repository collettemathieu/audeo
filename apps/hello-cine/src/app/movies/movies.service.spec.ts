import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Actor } from './entities/actor.entity';
import { Movie } from './entities/movie.entity';
import { MOVIE_RELEASE_DATE } from './movie.static';
import { MoviesService } from './movies.service';

export type MockRepository<T> = Partial<Record<keyof Repository<T>, jest.Mock>>;
export const createMockRepository = <T>(): MockRepository<T> => {
  return {
    create: jest.fn(),
    findOne: jest.fn()
  }
}

describe('MoviesService', () => {
  let service: MoviesService;
  let movieRepository: MockRepository<Movie>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        {
          provide: MOVIE_RELEASE_DATE,
          useValue: []
        },
        {
          provide: getRepositoryToken(Movie),
          useValue: createMockRepository<Movie>()
        },
        {
          provide: getRepositoryToken(Actor),
          useValue: createMockRepository<Actor>()
        }
      ],
    }).compile();

    service = module.get<MoviesService>(MoviesService);
    movieRepository = module.get<MockRepository<Movie>>(getRepositoryToken(Movie));

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {

    describe('when movie with #id exists', () => {
      it('should return a movie object', async () => {
        const movieId = 1;
        const expectedMovie = {};

        movieRepository.findOne.mockReturnValue(expectedMovie);
        
        const movie = await service.findOne(movieId);

        expect(movie).toEqual(expectedMovie);

      })
    });

    describe('otherwise', () => {

      describe('when movie does not exist and movieRepository return undefined', () => {
        it('should throw the "NotFoundException', async () => {
          const movieId = 1;
  
          movieRepository.findOne.mockReturnValue(undefined);
  
          try{
            await service.findOne(movieId);
          }catch(error){
            expect(error).toBeInstanceOf(NotFoundException);
            expect(error.message).toEqual(`Movie #${movieId} not found`);
          }
        })
      });

      describe('when movie does not exist and movieRepository return null', () => {
        it('should throw the "NotFoundException', async () => {
          const movieId = 1;
  
          movieRepository.findOne.mockReturnValue(null);
  
          try{
            await service.findOne(movieId);
          }catch(error){
            expect(error).toBeInstanceOf(NotFoundException);
            expect(error.message).toEqual(`Movie #${movieId} not found`);
          }
        })
      })
      
    });
  });
});
