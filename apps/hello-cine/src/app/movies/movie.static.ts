import { InjectionToken } from "@nestjs/common";

export const MOVIE_RELEASE_DATE = 'MOVIE_RELEASE_DATE';

export const DATA_SOURCE = 'DATA_SOURCE' as InjectionToken; 

