import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Movie } from "./movie.entity";

@Entity()
export class Actor {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    firstname: string;
    
    @Column()
    lastname: string;

    @Column()
    nickname: string;

    @Column()
    age: number;
    
    @ManyToMany(
        type => Movie,
        movie => movie.actors
    )
    movies: Movie[];

}
