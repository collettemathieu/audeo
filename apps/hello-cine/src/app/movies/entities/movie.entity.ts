import { Column, CreateDateColumn, Entity, Index, JoinTable, ManyToMany, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from "typeorm";
import { Actor } from "./actor.entity";

@Index(['title', 'releaseDate'])
@Entity()
export class Movie {

    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    @Index()
    title: string;

    @Column()
    releaseDate: Date;

    @JoinTable()
    @ManyToMany(
        type => Actor,
        actor => actor.movies,
        {
            cascade: true
        }
    )
    actors: Actor[];

    @CreateDateColumn()
    createAt: Date;

    @UpdateDateColumn()
    updateAt: Date;

    @VersionColumn({ default: 1})
    version: number;

}
