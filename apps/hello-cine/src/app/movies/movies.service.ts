import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { ConfigService, ConfigType } from '@nestjs/config';
import { REQUEST } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaginationQueryDto } from '../common/dtos/pagination-query.dto';
import { CreateActorDto } from './dtos/create-actor.dto';
import { CreateMovieDto } from './dtos/create-movie.dto';
import { UpdateMovieDto } from './dtos/update-movie.dto';
import { Actor } from './entities/actor.entity';
import { Movie } from './entities/movie.entity';
import { MOVIE_RELEASE_DATE } from './movie.static';
import moviesConfig from './movies.config';

@Injectable({
    scope: Scope.DEFAULT
})
export class MoviesService {

    constructor(
        @InjectRepository(Movie)
        private readonly movieRepository: Repository<Movie>,
        @InjectRepository(Actor)
        private readonly actorRepository: Repository<Actor>,
        @Inject(MOVIE_RELEASE_DATE) moviesReleaseDate: string[],
        // @Inject(REQUEST) private request: Request,
        // @Inject(moviesConfig.KEY)
        // private moviesConfiguration: ConfigType<typeof moviesConfig> 
    ){

        // const host = this.configService.get<string>('POSTGRES_HOST', 'default_value');

        // console.log({host});
        

        // const databaseHost = this.configService.get('database.host');

        // console.log({databaseHost});
        

        // const movies = this.moviesConfiguration.foo

        // console.log({movies});
        

        // console.log({moviesReleaseDate});

    }

    findAll(paginationQuery: PaginationQueryDto){
        const { limit, offset } = paginationQuery;
        return this.movieRepository.find({
            relations: ['actors'],
            take: limit,
            skip: offset
        });
    }

    async findOne(id: number){
        const movie =  await this.movieRepository.findOne({
            where: {id},
            relations: ['actors']
        });

        if(movie === undefined || movie === null){
            throw new NotFoundException(`Movie #${id} not found`);
        }

        return movie;
    }

    async create(createMovieDto: CreateMovieDto){
        const actors = await Promise.all(createMovieDto.actors.map((actor) => 
            this.preloadActorByNickname(actor)
        ));

        const movie = this.movieRepository.create({...createMovieDto, actors});
        return this.movieRepository.save(movie);
    }

    async update(id: number, updateMovieDto: UpdateMovieDto){

        const actors = updateMovieDto.actors !== undefined && await Promise.all(updateMovieDto.actors.map((actor) => 
            this.preloadActorByNickname(actor)
        ));

        const movie = await this.movieRepository.preload({
            id, 
            ...updateMovieDto,
            actors
        })

        if(movie === undefined || movie === null){
            throw new NotFoundException(`Movie #${id} not found`);
        }

        return this.movieRepository.save(movie);
    }

    async remove(id: number){
        const movie = await this.findOne(id);
        return this.movieRepository.remove(movie);
    }

    private async preloadActorByNickname(createActorDto: CreateActorDto){
        const actor =  createActorDto.nickname !== undefined && await this.actorRepository.findOne({
            where: {
                nickname: createActorDto.nickname
            }
        });

        if(actor !== undefined && actor !== null){
            return actor;
        }

        return this.actorRepository.create(createActorDto);
    }
}
