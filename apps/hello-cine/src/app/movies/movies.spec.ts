import { HttpServer, HttpStatus, INestApplication } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MoviesModule } from './movies.module';
import * as request from 'supertest';
import { CreateMovieDto } from "./dtos/create-movie.dto";

describe('[Feature] Movies - /movies', ()=>{

    let app: INestApplication;
    let httpServer: HttpServer;

    const movieDto: CreateMovieDto = {
        title:"Basic instinct",
        releaseDate: new Date("2023-03-03T08:04:15.863Z"),
        actors: [
          {
            firstname: "Mickael",
            lastname: "Douglas",
            age: 50,
            nickname:  "mdouglas"
          },
            {
            firstname: "Sharon",
            lastname: "Stone",
            age: 45,
            nickname: "sstone"
          }
        ]
      }
    
    beforeAll(async ()=>{
        const module: TestingModule = await Test.createTestingModule({
          imports:[
            MoviesModule,
            TypeOrmModule.forRoot({
              type: 'postgres',
              host: 'localhost',
              port: 5433,
              username: 'admin',
              password: 'secret',
              database: 'postgres',
              autoLoadEntities: true,
              synchronize: true,
            }),
          ]
        }).compile();
    
        app = module.createNestApplication();
        await app.init();
    
        httpServer = app.getHttpServer();
    
      });

      it('Create [POST /]', () => {
        return request(httpServer)
        .post('/movies')
        .send(movieDto)
        .expect(HttpStatus.CREATED)
        .then(({body}) => {
            const expectedMovie = expect.objectContaining({
                ...movieDto,
                releaseDate: movieDto.releaseDate.toISOString(),
                actors: expect.arrayContaining(movieDto.actors.map((actor) => 
                    expect.objectContaining({
                        ...actor
                    })
                ))
            });

            expect(body).toEqual(expectedMovie);
        })
      })

      afterAll(async () => {
        await app.close();
      })
});