import { Body, Controller, Delete, Get, HttpCode, HttpException, HttpStatus, NotFoundException, Param, Patch, Post, Query, Req, Res, SetMetadata, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { PaginationQueryDto } from '../common/dtos/pagination-query.dto';
import { CreateMovieDto } from './dtos/create-movie.dto';
import { UpdateMovieDto } from './dtos/update-movie.dto';
import { MoviesService } from './movies.service';
import { Request } from 'Express';
import { IsPublic } from '../common/decorators/is-public.decorator';
import { Reflector } from '@nestjs/core';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { Protocol } from '../common/decorators/protocol.decorator';
import { ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('movies')
@Controller({
    path: 'movies'
})
export class MoviesController {

    constructor(private moviesService: MoviesService){}

    @Get()
    // @IsPublic()
    findAll(@Protocol('https') protocol: string, @Query() paginationQuery: PaginationQueryDto){

        console.log({protocol});
        return this.moviesService.findAll(paginationQuery);
    }

    @ApiForbiddenResponse()
    @Get(':id')
    findOne(@Param('id', ParseIntPipe) id: number, @Req() request: Request){
        const movie = this.moviesService.findOne(id);

        if(movie === undefined || movie === null){
            // throw new HttpException(`Movie #${id} not found`, HttpStatus.NOT_FOUND);
            throw new NotFoundException(`Movie #${id} not found`);
        }

        return movie;
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() body: CreateMovieDto){
        return this.moviesService.create(body);
    }

    @Patch(':id')
    update(@Param('id') id: number, @Body() body: UpdateMovieDto){
        return this.moviesService.update(id, body);
    }

    @Delete(':id')
    remove(@Param('id') id: number){
        return this.moviesService.remove(id);
    }
}
