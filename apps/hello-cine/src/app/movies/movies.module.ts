import { Module, ValidationPipe } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Actor } from './entities/actor.entity';
import { Movie } from './entities/movie.entity';
import { DATA_SOURCE, MOVIE_RELEASE_DATE } from './movie.static';
import { MoviesController } from './movies.controller';
import { MoviesService } from './movies.service';
import { environment } from '../../environments/environment';
import { DataSource } from 'typeorm';
import { APP_PIPE } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';
import moviesConfig from './movies.config';


// export class ConfigService{}
// export class ConfigServiceProduction{}
// export class ConfigServiceDevelopment{}


export class MoviesReleaseDateFactory{
    create(){
        return ['2023', '2022', '2021']
    }
}

@Module({
    imports: [
        TypeOrmModule.forFeature([Movie, Actor]),
        ConfigModule.forFeature(moviesConfig)
    ],
    controllers: [MoviesController],
    providers: [MoviesService, MoviesReleaseDateFactory,
        // {
        //     provide: ConfigService,
        //     useClass: environment.production === true ? ConfigServiceProduction : ConfigServiceDevelopment
        // },
        {
            provide: DATA_SOURCE,
            useFactory: async () =>{
                const dataSource = new DataSource({
                  type: 'postgres', // type of our database
                  host: process.env.POSTGRES_HOST, // database host
                  port: +process.env.POSTGRES_PORT, // database host
                  username: process.env.POSTGRES_USER, // username
                  password: process.env.POSTGRES_PASSWORD, // user password
                  database: process.env.POSTGRES_DB, // name of our database,
                });
                await dataSource.initialize();
                return dataSource;
            }
        },
        {
            provide: MOVIE_RELEASE_DATE,
            useFactory: async (dataSource: DataSource) => {
                const actors = await dataSource.query("SELECT * FROM actor;");
                return actors;
            },
            inject: [DataSource]
        },
        {
            provide: APP_PIPE,
            useFactory: () => {
                return new ValidationPipe({
                    whitelist: true,
                    forbidNonWhitelisted: true,
                    // transform: true
                  })
            }
        }
    ],
    exports: [MoviesService]
})
export class MoviesModule {}
