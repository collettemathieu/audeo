import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsEmpty, IsNumber, IsString, MinLength } from "class-validator";

export class CreateActorDto {

    @ApiProperty({
        description: 'this is the firsname',
        example: 'toto'
    })
    @IsString()
    readonly firstname: string;
    
    @IsString()
    readonly lastname: string;

    @IsString()
    readonly nickname: string;

    @IsNumber()
    @Type(()=> Number)
    readonly age: number;
}
