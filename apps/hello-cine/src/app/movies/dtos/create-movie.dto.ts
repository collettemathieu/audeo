import { Type } from "class-transformer";
import { IsArray, IsDate, IsString, ValidateNested } from "class-validator";
import { CreateActorDto } from "./create-actor.dto";

export class CreateMovieDto {

    @IsString()
    readonly title: string;

    @IsDate()
    @Type(() => Date)
    readonly releaseDate: Date;

    @Type(() => CreateActorDto)
    @IsArray()
    @ValidateNested()
    readonly actors: CreateActorDto[];
}

