import { Module } from '@nestjs/common';
import { MoviesModule } from '../movies/movies.module';
import { MovieRatingService } from './movie-rating.service';

@Module({
  imports: [MoviesModule],
  providers: [MovieRatingService],
})
export class MovieRatingModule {}
